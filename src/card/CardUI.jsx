import React from 'react';
import admin from '../images/admin.svg';
import analysis from '../images/analysis.svg';
import architecture from '../images/architecture.svg';
import customer from '../images/customer.svg';
import design from '../images/design.svg';
import engineering from '../images/engineering.svg';
import sales from '../images/sales.svg';
import web from '../images/web.svg';
import writing from '../images/writing.svg';
import './card-style.css'
import Analytics from '../pages/analytics-data-science'


const Card= props =>{
	return (

	<div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0px 1.0875rem 1.45rem`,
          paddingTop: 0,
        }}
      >

	 <div className="card text-center shadow ">
	 {/* Analytics & Data Science */}
	 	<div className="overflow">
	 		<img src={analysis} alt="Analytics & Data Science" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
		 		<a href="/analytics-data-science">
		 		Analytics & Data Science</a>
	 		</h4>

	 	</div>

	 	 {/* Design & Creative */}
		<div className="overflow">
	 		<img src={design} alt="Design & Creative" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/design-creative">
	 		Design & Creative
	 		</a>
	 		</h4>
	 		 
	 	</div>

	 	{/* Admin Support */}
	 	<div className="overflow">
	 		<img src={admin} alt="Admin Support" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/admin-support">
	 		Admin Support
	 		</a>
	 		</h4>
	 	</div>

	 	{/* Customer Service */}
	 	<div className="overflow">
	 		<img src={customer} alt="Customer Service" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/customer-service">Customer Service
	 		</a>
	 		</h4>

	 	</div>

	 	{/* Writing */}
	 	<div className="overflow">
	 		<img src={writing} alt="Writing" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/writing">
	 		Writing
	 		</a>
	 		</h4>
	 	</div>

	 	{/* Sales & Marketing */}
	 	<div className="overflow">
	 		<img src={sales} alt="Sales & Marketing" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/sales-marketing">
	 		Sales & Marketing
	 		</a>
	 		</h4>
	 	</div>

	 	{/* Web,Mobile & Software dev */}
	 	<div className="overflow">
	 		<img src={web} alt="Web,Mobile & Software dev" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/web-mobile-software">
	 		Web,Mobile & Software dev
	 		</a>
	 		</h4>
	 	</div>

        {/*Architecture */}
	 	<div className="overflow">
	 		<img src={architecture} alt="Architecture" className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/architecture">
	 		Architecture
	 		</a>
	 		</h4>
	 	</div>

	 	{/*Engineering  */}
	 	<div className="overflow">
	 		<img src={engineering} alt="Engineering " className="card-img-top"/>
	 	</div>

	 	<div className="card-body text-dark">
	 		<h4 className="card-title">
	 		<a href="/engineering">
	 		Engineering 
	 		</a>
	 		</h4>
	 	</div>

	 	

	 </div>
	 </div>

	);
}


export default Card;