import React from 'react'
import Navbar from "../components/Globals/Navbar";
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-responsive-list'
import 'react-responsive-list/assets/index.css'

const Analytics = () => {
	return(		
<div>
<Navbar/>
<Table breakPoint={700}>
  <Thead>
  	<Tr>
            <Th>Job Title</Th>  
            <Th>Job Description</Th>
            <Th>Category</Th>
            <Th>Payment</Th>
            <Th>Deadline</Th>
            <Th></Th>
           
        </Tr>
    </Thead>
    <Tbody>

    <Tr>
            <Td>Analyst</Td>
             <Td>Small automated and analytical business</Td>
            <Td>Analytics</Td>
            <Td>Not disclosed</Td>
            <Td>20-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>

        <Tr>
            <Td>Analyst</Td>
             <Td>Small automated and analytical business</Td>
            <Td>Analytics</Td>
            <Td>Not disclosed</Td>
            <Td>20-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>
        </Tbody>
</Table>


    </div>
		)
}

export default Analytics;
