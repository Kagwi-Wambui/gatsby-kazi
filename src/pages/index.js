import React from "react"
import { Link, graphql } from "gatsby"
import Layout from "../components/layout"
import BackgroundSection from "../components/Globals/BackgroundSection"
import SEO from "../components/seo"
export { Login } from "../components/login/login"
export { Register } from "../components/login/register"


const IndexPage = ({data}) => (
  <Layout>
    <SEO title="Home" />
   
    <BackgroundSection img=
    {data.img.childImageSharp.fluid} 
    title="Connect & Collaborate"
    subtitle="Pata Kazi makes it easy for quality employers and freelancers to connect, collaborate, and get work done flexibly and securely."
    styleClass="default-background"
    />
    

  </Layout>
);

export const query = graphql`
{
  img:file(relativePath:{eq: "bg_op.png"}){
    childImageSharp{
      fluid{
        src
      }
    }
  }
}


`
export default IndexPage
