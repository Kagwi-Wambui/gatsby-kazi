import React from 'react'
import Navbar from "../components/Globals/Navbar";
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-responsive-list'
import 'react-responsive-list/assets/index.css'

const WebMob = () => {
	return(
	<div>
		<Navbar/>

<Table breakPoint={700}>
  <Thead>
  	<Tr>
            <Th>Job Title</Th>  
            <Th>Job Description</Th>
            <Th>Category</Th>
            <Th>Payment</Th>
            <Th>Deadline</Th>
            <Th></Th>
           
        </Tr>
    </Thead>
    <Tbody>

    <Tr>
            <Td>Web developer</Td>
            <Td>Create a blog website for a fashion company</Td>
            <Td>Web & Mobile Dev</Td>
            <Td>Fixed</Td>
            <Td>13-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>
 </Tbody>
</Table>


	</div>	
		)
}

export default WebMob;