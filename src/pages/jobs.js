import React from 'react'
import Navbar from "../components/Globals/Navbar";
import JobsNavbar from "../components/Globals/JobsNavbar";
import JobSearch from "../components/jobsearchbar";
import FilterBtn from "../components/filterbutton";
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-responsive-list'
import 'react-responsive-list/assets/index.css'

const Jobs = () => {
	return(
	<div>
		<Navbar/>
		<JobsNavbar/>

		<hr  style={{
		    color: '#000000',
		    backgroundColor: '#000000',
		    height: .5,
		    borderColor : '#000000'
		}}/>

		<FilterBtn/>

<Table breakPoint={700}>
  <Thead>
  	<Tr>
            <Th>Job Title</Th>  
            <Th>Job Description</Th>
            <Th>Category</Th>
            <Th>Payment</Th>
            <Th>Deadline</Th>
            <Th></Th>
           
        </Tr>
    </Thead>
    <Tbody>
        
        <Tr>
            <Td>Writer</Td>
            <Td>Ghostwriter needed for a 25 page fiction</Td>
            <Td>Writing</Td>
            <Td>Hourly</Td>
            <Td>15-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>
        <Tr>
            <Td>Analyst</Td>
             <Td>Small automated and analytical business</Td>
            <Td>Analytics</Td>
            <Td>Not disclosed</Td>
            <Td>20-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>
        <Tr>
            <Td>Graphic Designer</Td>
            <Td>Corporate Graphic designer</Td>
            <Td>Design</Td>
            <Td>Fixed</Td>
            <Td>17-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>
        <Tr>
            <Td>Analyst</Td>
             <Td>Small automated and analytical business</Td>
            <Td>Analytics</Td>
            <Td>Not disclosed</Td>
            <Td>20-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>
    </Tbody>
</Table>

		</div>
		)
}

export default Jobs;
