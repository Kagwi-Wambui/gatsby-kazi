import React from 'react'
import Navbar from "../components/Globals/Navbar";
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-responsive-list'
import 'react-responsive-list/assets/index.css'

const Writing = () => {
	return(
	<div>
		<Navbar/>
	
<Table breakPoint={700}>	
	<Thead>
  	<Tr>
            <Th>Job Title</Th>  
            <Th>Job Description</Th>
            <Th>Category</Th>
            <Th>Payment</Th>
            <Th>Deadline</Th>
            <Th></Th>
           
        </Tr>
    </Thead>
    <Tbody>

    <Tr>
            <Td>Writer</Td>
            <Td>Ghostwriter needed for a 25 page fiction</Td>
            <Td>Writing</Td>
            <Td>Hourly</Td>
            <Td>15-11-2019</Td>
            <Td><button className="btn btn-blue">Send Proposal</button></Td>
        </Tr>

         </Tbody>
	</Table>
</div>
		)
}

export default Writing;