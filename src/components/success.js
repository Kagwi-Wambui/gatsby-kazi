import React, { Component } from 'react'
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import AppBar from '@material-ui/core/AppBar';

export class Success extends Component{
	continue = e => {
		e.preventDefault();

		//process form(send to API)
		this.props.nextStep();
	}

	back = e => {
		e.preventDefault();
		this.props.prevStep();
	}

	render(){
		return(
			<ThemeProvider>
				<React.Fragment>
				<AppBar title= "Success" />

				<h1>Thank you for your submission</h1>
				<p>You will get an email further instructions</p> 
				
				</React.Fragment>
			</ThemeProvider>

			);
	}
}


export default Success