import React, { Component } from 'react'
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import AppBar from '@material-ui/core/AppBar';
import { List, ListItem } from '@material-ui/core/List';
import Button from '@material-ui/core/Button';

export class Confirm extends Component{
	continue = e => {
		e.preventDefault();

		//process form(send to API)
		this.props.nextStep();
	}

	back = e => {
		e.preventDefault();
		this.props.prevStep();
	}

	render(){
		const { values: {jobTitle, jobDescription, category, paymentTerms, amount} } = this.props;

		return(
			<ThemeProvider>
				<React.Fragment>
				<AppBar title= "Confirm Details" />

					<List>
						<ListItem
						primaryText="Job Title"
						secondaryText={ jobTitle }
						/>

						<ListItem
						primaryText="Job Description"
						secondaryText={ jobDescription}
						/>

						<ListItem
						primaryText="Category"
						secondaryText={ category }
						/>

						<ListItem
						primaryText="Payment Terms"
						secondaryText={ paymentTerms }
						/>

						<ListItem
						primaryText="Amount"
						secondaryText={ amount }
						/>
					</List>
					<br/>

					<Button
					label="Confirm & Continue"
					primary={true}
					style={styles.button}
					onClick={this.continue}
					/>

					<Button
					label="Back"
					primary={false}
					style={styles.button}
					onClick={this.back}
					/>
				</React.Fragment>
			</ThemeProvider>

			);
	}
}

const styles = {
	button : {
		margin: 15
	}
}
export default Confirm