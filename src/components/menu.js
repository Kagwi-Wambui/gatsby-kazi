import React from 'react'
import Link from 'gatsby-link';

const Menu = () => {
	return(
		<div style={{
			background: '#F6F5F5',
			paddingTop: '10px'
		}}>

		<ul style={{
			listStyle: 'none',
			display: 'flex',
			justifyContent: 'space-evenly'
		}}>
			<li><Link to="/">Home</Link></li>
			<li><Link to="/post-a-job">Post A Job</Link></li>
			<li><Link to="/find-job">Find a Job</Link></li>
			<li><Link to="/freelancers">Freelancers</Link></li>
			<li><Link to="/how-it-works">How it Works</Link></li>
		</ul>
		</div>

		)
}

export default Menu; 