import React, { Component } from 'react'
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export class FormJobDetails extends Component{
	continue = e => {
		e.preventDefault();
		this.props.nextStep();
	}

	render(){
		const { values, handleChange } = this.props;

		return(
			<ThemeProvider>
				<React.Fragment>
				<AppBar title= "Post a Job" />

					<TextField 
					hintText="Job Title"
					floatingLabelText="Job Title"
					onChange={handleChange('jobTitle')}
					defaultValue={values.jobTitle}
					/>

					<br/>

					<TextField 
					hintText="Job Description"
					floatingLabelText="Job Description"
					onChange={handleChange('jobDescription')}
					defaultValue={values.jobDescription}
					/>

					<br/>

					<TextField 
					hintText="Category"
					floatingLabelText="Category"
					onChange={handleChange('category')}
					defaultValue={values.category}
					/>

					<br/>

					<Button
					label="Continue"
					primary={true}
					style={styles.button}
					onClick={this.continue}
					/>

				</React.Fragment>
			</ThemeProvider>

			);
	}
}

const styles = {
	button : {
		margin: 15
	}
}
export default FormJobDetails