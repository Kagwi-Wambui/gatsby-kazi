import React, {Component} from 'react'
import { FormJobDetails } from './FormJobDetails'
import { JobPaymentForm } from './jobpaymentform'
import Confirm from './confirm'
import Success from './success'

export class JobFormDetails extends Component{
 state= {
 	step:1,
 	jobTitle: '',
 	jobDescription: '',
 	category: '',
 	paymentTerms: '',
 	amount:''
 }

 //Proceed to next step
 nextStep = () => {
 	const {step} = this.sate;
 	this.setState({
 		step: step + 1
 	});
 }

 //Go back to previous step
 prevStep = () => {
 	const {step} = this.sate;
 	this.setState({
 		step: step - 1
 	});
 }

//Handle fields change
handleChange = input => e =>{
	this.setState({[input]: e.target.value});
}

	render(){
		const {step} =this.state;
		const {jobTitle,jobDescription,category} =this.state;
		const values = {jobTitle,jobDescription,category}


		switch (step){
			case 1:
			return (
				<FormJobDetails
					nextStep= {this.nextStep}
					handleChange={this.handleChange}
					values={values}
				/>
				)

			case 2:
			return (
				<JobPaymentForm
					nextStep= {this.nextStep}
					prevStep= {this.prevStep}
					handleChange={this.handleChange}
					values={values}
				/>
				)

			case 3:
			return (
				<Confirm
					nextStep= {this.nextStep}
					prevStep= {this.prevStep}
					values={values}
				/>
				)

			case 4:
			return <Success/>;
		}
	}
}

export default JobFormDetails