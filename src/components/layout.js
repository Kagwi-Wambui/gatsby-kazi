import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import  ReactDOM  from "react-dom"
//import HowWorks from "../components/how-works"
import 'bootstrap/dist/css/bootstrap.min.css'
import Card from '../card/CardUI';
import "./layout.css"
import Navbar from "./Globals/Navbar";
import Footer from "./Globals/Footer";
//import { hot } from "react-hot-loader";

const Layout = ({ children }) => {

  return (
    <>
      
      <Navbar/>
      <div
        style={{
          // margin: `0 auto`,
          // maxWidth: 960,
          // padding: `0px 1.0875rem 1.45rem`,
          // paddingTop: 0,
        }}
      >
        <main>
        {children}

         <h1 align="center">Choose Category</h1>

        <div id="root"></div>
        <Card/>
         <h1 align="center">How it Works</h1>
        


       
        </main>


   

        
        <Footer/>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

 


export default Layout;
