import React, { Component } from 'react'
import ThemeProvider  from '@material-ui/styles/ThemeProvider';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export class JobPaymentForm extends Component{
	continue = e => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = e => {
		e.preventDefault();
		this.props.prevStep();
	}


	render(){
		const { values, handleChange } = this.props;

		return(
			<ThemeProvider>
				<React.Fragment>
				<AppBar title= "Post a Job" />

					<TextField 
					hintText="Payment Terms"
					floatingLabelText="Payment Terms"
					onChange={handleChange('paymentTerms')}
					defaultValue={values.paymentTerms}
					/>

					<br/>

					<TextField 
					hintText="Amount"
					floatingLabelText="Amount"
					onChange={handleChange('amount')}
					defaultValue={values.amount}
					/>

					<br/>


					<Button
					label="Continue"
					primary={true}
					style={styles.button}
					onClick={this.continue}
					/>

					<Button
					label="Back"
					primary={false}
					style={styles.button}
					onClick={this.back}
					/>

				</React.Fragment>
			</ThemeProvider>

			);
	}
}

const styles = {
	button : {
		margin: 15
	}
}
export default JobPaymentForm