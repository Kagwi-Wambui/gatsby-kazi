import React from 'react'

function Footer(){
	return(
		<div className="main-footer">
			<div className="container-footer">
			<div className="row">
			{/*Column 1*/}
			<div className="col-md-3 col-sm-6 white-text">
			<h4>Job Seekers & Recruiters</h4>
				<ul className="list-unstyled">
					<li>Find a job</li>
					<li>Post a job</li>
					<li>Find freelancers</li>
					<li>Articles & Blogs</li>
				</ul>
			</div>

			{/*Column 2*/}
			<div className="col-md-3 col-sm-6 white-text">
			<h4>About</h4>
				<ul className="list-unstyled">
					<li>FAQs</li>
					<li>Contact Us</li>
					<li>Privacy Policy</li>
					<li>Terms of Use</li>
				</ul>
			</div>

			{/*Column 3*/}
			<div className="col-md-3 col-sm-6 white-text">
			<h4>Social Media</h4>
				<ul className="list-unstyled">
					<li>FAQs</li>
					<li>Contact Us</li>
					<li>Privacy Policy</li>
					<li>Terms of Use</li>
				</ul>
			</div>
			</div>


		<hr  style={{
		    color: '#ffffff',
		    backgroundColor: '#ffffff',
		    height: .5,
		    borderColor : '#000000'
		}}/>

		{/*Bottom Description*/}
		<div className="row right-content">
		<div className="col-md-8 white-text">
			<p>
			Pata Kazi offers freelancers, ongoing university and
			college students a chance to work on projects with
			professionalism. This ensures clients get quality
			services and the students get experience to improve
			their skills.
			</p>
		</div>
		</div>
		
		{/*Bottom*/}
			<div className="footer-bottom">
				<p className="text-xs-center white-text">
				 &copy; Copyright {new Date().getFullYear().toString()} Pata Kazi KE
				</p>
			</div>
			


			</div>
		</div>
		);
}


export default Footer