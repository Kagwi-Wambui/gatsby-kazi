import React, { Component} from 'react';
//import {FaBriefcase} from 'react-icons/fa';
import Link from 'gatsby-link';
import 'bootstrap/dist/css/bootstrap.min.css'

export default class Navbar extends Component {
state ={
	navbarOpen:false,
	css:'collapse navbar-collapse',
	links: [
	{
		id:1,
		path:'/',
		text: 'home'
	},

	{
		id:2,
		path:'/post-a-job',
		text: 'post a job'
	},

	{
		id:3,
		path:'/freelancers',
		text: 'freelancers'
	},

	{
		id:4,
		path:'/jobs',
		text: 'jobs'
	},

	{
		id:5,
		path:'/how-it-works',
		text: 'how it works'
	}

	]
}

navbarHandler = () => {
 this.state.navbarOpen?this.setState(
 {navbarOpen: false,css:"collapse navbar-collapse"})
 :this.setState({
 navbarOpen: true,
 css: "collapse navbar-collapse show"
 });
};

render(){
		return (
            <nav className="navbar navbar-expand-sm bg-light navbar-light">
            	

            	<button 
            	className="navbar-toggler" 
            	type="button" 
            	onClick={this.navbarHandler}
            	>

            	<span className="navbar-toggler-icon"/>
            	</button>


            	<div className={this.state.css}>
            	<ul className="navbar-nav mx-auto">
            	{
            		this.state.links.map(link=>{
            			return(
            			<li key={link.id} className="nav-item">
            			<Link to={link.path} className="nav-link 
            			text-capitalize">
            			{link.text}
            			</Link>
            			</li>

            			);
            		})}

            		
            		{/* 
				  	<li className="nav-item ml-sm-5">
            		<FaBriefcase className="job-icon"/>
            		</li>
				*/}  
            	</ul>

            	<Link to ="/sign-in" className="navbar-brand">
            	<button className="btn btn-blue">Sign In</button>
            	{/* <img src={logo} alt="logo"/> */}
            	
            	</Link>
            	</div>

            </nav> 
			);
	}
	
}