import React, { Component} from 'react';
import BackgroundImage from 'gatsby-background-image';

export default function BackgroundSection ({
	img,
	styleClass,
	title,
	subtitle,
	children}) {

return( <BackgroundImage className={styleClass} fluid={img}>

	<h1 className="title text-white text-center font-weight-bold">
	{title}
	</h1>

	<br/>

	
	

	</BackgroundImage>
	// {children} 
	
	) ;
}

BackgroundSection.defaultProps ={
	title: "default title",
	styleClass: "default-background"
};