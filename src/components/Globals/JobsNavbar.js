import React, { Component} from 'react';
//import {FaBriefcase} from 'react-icons/fa';
import Link from 'gatsby-link';
import 'bootstrap/dist/css/bootstrap.min.css'

export default class JobsNavbar extends Component {
state ={
	navbarOpen:false,
	css:'collapse navbar-collapse',
	links: [
	{
		id:1,
		path:'/writing',
		text: 'writing'
	},

	{
		id:2,
		path:'/customer-service',
		text: 'customer service'
	},

	{
		id:3,
		path:'/engineering',
		text: 'engineering'
	},

	{
		id:4,
		path:'/design-creative',
		text: 'design'
	},

	{
		id:5,
		path:'/sales-marketing',
		text: 'sales'
	},

	{
		id:6,
		path:'/analytics-data-science',
		text: 'analytics'
	},

	{
		id:7,
		path:'/admin-support',
		text: 'support'
	},

	{
		id:8,
		path:'/web-mobile-software',
		text: 'web & mobile dev'
	},

	{
		id:8,
		path:'/architecture',
		text: 'architecture'
	},

	{
		id:9,
		path:'/jobs',
		text: 'all categories'
	}

	]
}

navbarHandler = () => {
 this.state.navbarOpen?this.setState(
 {navbarOpen: false,css:"collapse navbar-collapse"})
 :this.setState({
 navbarOpen: true,
 css: "collapse navbar-collapse show"
 });
};

render(){
		return (
            <nav className="navbar navbar-expand-sm navbar-light">
            	

            	<button 
            	className="navbar-toggler" 
            	type="button" 
            	onClick={this.navbarHandler}
            	>

            	<span className="navbar-toggler-icon"/>
            	</button>


            	<div className={this.state.css}>
            	<ul className="navbar-nav mx-auto">
            	{
            		this.state.links.map(link=>{
            			return(
            			<li key={link.id} className="nav-item">
            			<Link to={link.path} className="nav-link 
            			text-capitalize">
            			{link.text}
            			</Link>
            			</li>

            			);
            		})}

            		
            		{/* 
				  	<li className="nav-item ml-sm-5">
            		<FaBriefcase className="job-icon"/>
            		</li>
				*/}  
            	</ul>

            	
            	</div>

            </nav> 
			);
	}
	
}